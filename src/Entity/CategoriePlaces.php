<?php

namespace App\Entity;

use App\Repository\CategoriePlacesRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: CategoriePlacesRepository::class)]
class CategoriePlaces
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private  $id = null;

    #[ORM\Column(type: Types::BIGINT)]
    private  $prix = null;

    #[ORM\Column(length: 255)]
    private  $libele = null;

    #[ORM\OneToMany(targetEntity: Places::class, mappedBy: 'categorie', orphanRemoval: true)]
    private  $places;

    public function __construct()
    {
        $this->places = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPrix(): ?string
    {
        return $this->prix;
    }

    public function setPrix(string $prix)
    {
        $this->prix = $prix;

        return $this;
    }

    public function getLibele(): ?string
    {
        return $this->libele;
    }

    public function setLibele(string $libele)
    {
        $this->libele = $libele;

        return $this;
    }

    /**
     * @return Collection<int, Places>
     */
    public function getPlaces(): Collection
    {
        return $this->places;
    }

    public function addPlace(Places $place)
    {
        if (!$this->places->contains($place)) {
            $this->places->add($place);
            $place->setCategorie($this);
        }

        return $this;
    }

    public function removePlace(Places $place)
    {
        if ($this->places->removeElement($place)) {
            // set the owning side to null (unless already changed)
            if ($place->getCategorie() === $this) {
                $place->setCategorie(null);
            }
        }

        return $this;
    }
}
